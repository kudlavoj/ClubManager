//? Core
import React from 'react';
import { Router, Route, Switch } from 'react-router';
//? Utils
import browserHistory from './services/history';
//? Components
import AdminScene from './scenes/Admin';
import AppScene from './scenes/App';
//? Styles
import 'antd/dist/antd.css';
import './styles/styles.scss';

export default () => (
	<Router history={browserHistory}>
		<Switch>
			<Route path="/admin" component={AdminScene} />
			<Route path="/" component={AppScene} />
		</Switch>
	</Router>
);
