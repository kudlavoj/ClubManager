// tslint:disable:no-any
interface LooseObject<T = any> {
	[key: string]: T;
}

interface WithRouterProps {
	match: any;
	history: any;
	location: any;
}

interface HookState<T> {
	error: Error | null;
	loading: boolean;
	data: T | null;
}

interface MongoNode {
	_id?: string;
}

type RelationID = string;

interface IUser extends MongoNode {
	name?: string;
	email?: string;
	phone?: string;
	address?: string;
	pass?: string;
	// Relations
	events?: Array<RelationID>;
	classInstances?: Array<RelationID>;
	memberships?: Array<RelationID>;
}

interface ICoach extends MongoNode {
	name?: string;
	// Relations
	classes?: Array<RelationID>;
	club?: RelationID;
}

interface IClub extends MongoNode {
	name?: string;
	description?: string;
	address?: string;
	// Relations
	coaches?: Array<RelationID>;
	classes?: Array<RelationID>;
	offers?: Array<RelationID>;
	events?: Array<RelationID>;
}

interface IOffer extends MongoNode {
	name?: string;
	description?: string;
	price?: number;
	maxClasses?: number;	
	// Relations
	club?: RelationID;
}

interface IMembership extends MongoNode {
	active?: boolean;
	expire?: Date;
	// Relations
	user?: RelationID;
	offer?: RelationID;
	club?: RelationID;
}

interface IClubClass extends MongoNode {
	active?: boolean;
	capacity?: number;
	type?: string;
	coach?: ID;
	club?: ID;
	// Relations
	instances?: Array<RelationID>;
}

interface IClassInstance extends MongoNode {
	start?: string;
	end?: string;
	// Relations
	clubClass?: RelationID;
}

interface IEvent extends MongoNode {
	name?: string;
	description?: string;
	capacity?: number;
	start?: Date;
	end?: Date;
	// Relations
	participants?: Array<RelationID>;
	club?: RelationID;
}
