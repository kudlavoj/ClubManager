import axios from 'axios';

interface Storage {
	clubs: Array<IClub>;
	coaches: Array<ICoach>;
	offers: Array<IOffer>;
}

class ApiService {

	private storage: Storage;
	private URL: string;

	constructor() {
		this.storage = {
			clubs: [],
			coaches: [],
			offers: []
		};
		this.URL = process.env.REACT_APP_API_SERVER;
	}

	public async getClubs(): Promise<Array<IClub>> {
		if (this.storage.clubs.length) {
			return this.storage.clubs;
		}
		return this.fetchClubs();
	}

	public async fetchClubs(): Promise<Array<IClub>> {
		const result = await axios.get<Array<IClub>>(`${this.URL}/clubs`)
			.then(response => response.data);
		this.storage.clubs = result;
		return result;
	}

	public async getClub(id: string): Promise<IClub> {
		const club = this.storage.clubs.find((item: IClub) => item._id === id);
		if (club) { return club; }
		return this.fetchClub(id);
	}

	public async fetchClub(id: string): Promise<IClub> {
		const result = await axios.get<IClub>(`${this.URL}/club/${id}`)
			.then(response => response.data);
		this.storage.clubs.unshift(result);
		return result;
	}

	public async deleteClub(id: string): Promise<boolean> {
		await axios.post<boolean>(`${this.URL}/deleteClub`, { where: { id } });
		this.storage.clubs = this.storage.clubs.filter(club => club._id !== id);
		return true;
	}

	public async updateClub(club: IClub): Promise<IClub> {
		const result = await axios.post<IClub>(`${this.URL}/updateClub`, { data: club, where: { id: club._id } })
			.then(response => response.data);
		this.storage.clubs = this.storage.clubs.map(item => {
			if (item._id === club._id) { return result; }
			return item;
		});
		return result;
	}

	public async createClub(club: IClub): Promise<IClub> {
		const result = await axios.post<IClub>(`${this.URL}/createClub`, { data: club })
			.then(response => response.data);
		this.storage.clubs.unshift(result);
		return result;
	}

	public async getCoaches(clubID: RelationID): Promise<Array<ICoach>> {
		if (!this.storage.coaches.length) { await this.fetchCoaches(); }
		const coaches = this.storage.coaches.filter(coach => coach.club === clubID);
		return coaches;
	}

	public async fetchCoaches(): Promise<Array<ICoach>> {
		const result = await axios.get<Array<ICoach>>(`${this.URL}/coaches`)
			.then(response => response.data);
		this.storage.coaches = result;
		return result;
	}

	public async createCoach(coach: ICoach): Promise<ICoach> {
		const result = await axios.post<ICoach>(`${this.URL}/createCoach`, { data: coach })
			.then(response => response.data);
		this.storage.coaches.unshift(coach);
		return result;
	}

	public async deleteCoach(id: string): Promise<boolean> {
		await axios.post<boolean>(`${this.URL}/deleteCoach`, { where: { id } });
		this.storage.coaches = this.storage.coaches.filter(coach => coach._id !== id);
		return true;
	}

	public async getOffers(clubID: RelationID): Promise<Array<IOffer>> {
		if (!this.storage.offers.length) { await this.fetchOffers(); }
		const offers = this.storage.offers.filter(offer => offer.club === clubID);
		return offers;
	}

	public async fetchOffers(): Promise<Array<IOffer>> {
		const result = await axios.get<Array<IOffer>>(`${this.URL}/offers`)
			.then(response => response.data);
		this.storage.offers = result;
		return result;
	}

	public async createOffer(offer: IOffer): Promise<IOffer> {
		const result = await axios.post<IOffer>(`${this.URL}/createOffer`, { data: offer })
			.then(response => response.data);
		this.storage.offers.unshift(result);
		return result;
	}

	public async updateOffer(offer: IOffer): Promise<IOffer> {
		const result = await axios.post<IOffer>(`${this.URL}/updateOffer`, { data: offer, where: { id: offer._id } })
			.then(response => response.data);
		this.storage.offers = this.storage.offers.map(item => {
			if (item._id === result._id) { return result; }
			return item;
		});
		return result;
	}

	public async deleteOffer(id: string): Promise<boolean> {
		await axios.post<boolean>(`${this.URL}/deleteOffer`, { where: { id } });
		this.storage.offers = this.storage.offers.filter(offer => offer._id !== id);
		return true;
	}

}

export default ApiService;
