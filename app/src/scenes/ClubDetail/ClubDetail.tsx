import React, { Component, ReactNode } from 'react';
import { Card, Row, Col } from 'antd';
import { Link } from 'react-router-dom';

import apiService from '../../services/api';

import Button from '../../components/AppButton';

interface Properties { }

interface State {
	club: IClub;
	offers: Array<IOffer>;
	loading: boolean;
}

class ClubDetail extends Component<Properties, State> {

	constructor(props: Properties) {
		super(props);
		this.state = {
			club: {},
			offers: [],
			loading: true
		};
	}

	componentDidMount(): void {
		this.getClubs();
	}

	getClubs(): void {
		const id = window.location.pathname.split('/')[2];
		Promise.all([
			apiService.getClub(id),
			apiService.getOffers(id)
		]).then(([club, offers]) => {
			this.setState({ club, offers, loading: false });
		});
	}

	render(): ReactNode {
		const { club, offers, loading } = this.state;
		if (loading) { return <Card loading={true} />; }
		console.log(offers);
		return (
			<>
				<div className="clubDetailHeader">
					<div className="appContainer">{club.name}</div>
				</div>
				<div className="appContainer">
					<div className="clubDetail">
						<div className="clubName">{club.name}</div>
						<Row>
							<Col span={12} className="clubAddress">
								<div className="rowTitle">Adresa</div>
								<div className="rowValue">{club.address}</div>
							</Col>
							<Col span={12} className="clubDescription">
								<div className="rowTitle">O Klubu</div>
								<div className="rowValue">{club.description}</div>
							</Col>
						</Row>
					</div>
					<div className="clubOffers">
						<div className="clubOffersTitle">Nabídky členství</div>
						{offers.map((offer, key) => {
							return (
								<div key={key} className="clubOffer">
									<div className="offerTitle">{offer.name}</div>
									<div className="offerDescription">{offer.description}</div>
									<div className="offerClasses">
										<span className="offerClassesTitle">Počet lekcí se členstvím: </span>
										<span className="offerClassesValue">{offer.maxClasses || 'Neomezeně'}</span>
									</div>
									<div className="offerPrice">
										<div className="offerPriceTitle">Cena</div>
										<div className="offerPriceValue">{offer.price} Kč</div>
									</div>
									<div className="offerBuy">
										<Button>Koupit</Button>
									</div>
								</div>
							);
						})}
					</div>
				</div>
			</>
		);
	}

}

export default ClubDetail;
