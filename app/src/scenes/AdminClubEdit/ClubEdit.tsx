import React, { Component, ReactNode } from 'react';
import { Card, Input, Button, notification } from 'antd';
import { Link } from 'react-router-dom';

import apiService from '../../services/api';

import InputWrap from '../../components/InputWrap';
import CoachPicker from '../../components/CoachPicker';
import OfferEditor from '../../components/OfferEditor';

interface Properties { }

interface State {
	club: IClub;
	loading: boolean;
	processing: boolean;
}

class ClubEdit extends Component<Properties, State> {

	constructor(props: Properties) {
		super(props);
		this.state = {
			club: {},
			loading: true,
			processing: false
		};
	}

	componentDidMount(): void {
		const id = window.location.pathname.split('/')[3];
		if (id === 'new') {
			this.setState({ loading: false });
		} else {
			apiService.getClub(id).then(club => this.setState({ club, loading: false }));
		}
	}

	// tslint:disable-next-line:no-any
	updateClubField = ({ key, value }: { key: string, value: any }) => {
		const { club } = this.state;
		club[key] = value;
		this.setState({ club });
	}

	updateClubRelation = async ({ key, value }: { key: string, value: Array<RelationID> }) => {
		const { club } = this.state;
		club[key] = value;
		try {
			await new Promise(r => this.setState({ processing: true }, r));
			const result = await apiService.updateClub(club);
			this.setState({ club: result });
		} catch (e) {
			notification.error({
				message: e.name,
				description: e.message
			});
		} finally {
			this.setState({ processing: false });
		}
	}

	saveClub = async () => {
		const { club } = this.state;
		this.setState({ processing: true });
		try {
			if (club._id) {
				const result = await apiService.updateClub(club);
				this.setState({ club: result });
			} else {
				const result = await apiService.createClub(club);
				this.setState({ club: result });
			}
			notification.success({
				message: 'Úspěch',
				description: 'Klub byl úspěšně uložen'
			});
		} catch (e) {
			notification.error({
				message: e.name,
				description: e.message
			});
		} finally {
			this.setState({ processing: false });
		}
	}

	render(): ReactNode {
		const { club, loading, processing } = this.state;
		if (loading) { return <Card loading={true} />; }
		return (
			<div style={{ paddingBottom: 20 }}>
				<InputWrap title="Název">
					<Input
						value={club.name}
						onChange={({ target: { value } }) => this.updateClubField({ key: 'name', value })}
					/>
				</InputWrap>
				<InputWrap title="Popis">
					<Input.TextArea
						value={club.description}
						autosize={{ minRows: 3 }}
						style={{ resize: 'none' }}
						onChange={({ target: { value } }) => this.updateClubField({ key: 'description', value })}
					/>
				</InputWrap>
				<InputWrap title="Adresa">
					<Input
						value={club.address}
						onChange={({ target: { value } }) => this.updateClubField({ key: 'address', value })}
					/>
				</InputWrap>
				{club._id && (
					<>
						<InputWrap title="Trenéři">
							<CoachPicker
								clubID={club._id}
								onChange={(value) => this.updateClubRelation({ key: 'coaches', value })}
							/>
						</InputWrap>
						<InputWrap title="Nabídky členství">
							<OfferEditor
								clubID={club._id}
								onChange={(value) => this.updateClubRelation({ key: 'offers', value })}
							/>
						</InputWrap>
					</>
				)}
				<div style={{ textAlign: 'right' }}>
					<Link to="/admin/clubs">
						<Button style={{ marginRight: 5 }}>Zpět</Button>
					</Link>
					<Button onClick={this.saveClub} type="primary" loading={processing}>Uložit</Button>
				</div>
			</div>
		);
	}

}

export default ClubEdit;
