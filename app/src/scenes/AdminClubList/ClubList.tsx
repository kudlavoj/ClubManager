import React, { Component, ReactNode } from 'react';
import { Table, Card, Button, notification, Modal } from 'antd';
import { Link } from 'react-router-dom';

import apiService from '../../services/api';

interface Properties { }

interface State {
	clubs: Array<IClub>;
	loading: boolean;
}

class ClubList extends Component<Properties, State> {

	constructor(props: Properties) {
		super(props);
		this.state = {
			clubs: [],
			loading: true
		};
	}

	componentDidMount(): void {
		this.getClubs();
	}

	getClubs(): void {
		apiService.fetchClubs().then(clubs => this.setState({ clubs, loading: false }));
	}

	deleteClub(club: IClub): void {
		Modal.confirm({
			title: <span>Opravdu chcete smazat klub <b>{club.name}</b>?</span>,
			content: 'Tato akce je nezvratná.',
			onOk: async () => {
				await apiService.deleteClub(club._id);
				const clubs = this.state.clubs.filter(item => item._id !== club._id);
				this.setState({ clubs });
				notification.success({
					message: 'Úspěch',
					description: 'Klub byl úspěšně smazán'
				});
			}
		});
	}

	render(): ReactNode {
		if (this.state.loading) { return <Card loading={true} />; }
		return (
			<>
				<Table
					dataSource={this.state.clubs.map((club, key) => ({ ...club, key }))}
					pagination={this.state.clubs.length > 10 ? { pageSize: 10 } : false}
				>
					<Table.Column title="Název" dataIndex="name" />
					<Table.Column title="Počet lekcí" render={(item: IClub) => item.classes.length} />
					<Table.Column title="Počet trenérů" render={(item: IClub) => item.coaches.length} />
					<Table.Column title="Počet událostí" render={(item: IClub) => item.events.length} />
					<Table.Column title="Počet nabídek" render={(item: IClub) => item.offers.length} />
					<Table.Column
						title="Akce"
						render={(club: IClub) => (
							<>
								<Link to={`/admin/club/${club._id}`}>
									<Button style={{ marginRight: 5 }}>Editovat</Button>
								</Link>
								<Button ghost={true} onClick={() => this.deleteClub(club)} type="danger">Smazat</Button>
							</>
						)}
					/>
				</Table>
				<div style={{ textAlign: 'center', padding: '20px 0' }}>
					<Link to="/admin/club/new">
						<Button type="primary">Nový klub</Button>
					</Link>
				</div>
			</>
		);
	}

}

export default ClubList;
