//? Core
import React from 'react';
import { Router, Route, Switch } from 'react-router';
//? Utils
import browserHistory from '../../services/history';
//? Components
import AppLayout from '../../components/AppLayout';
//? Scenes
import HomePage from '../HomePage';
import ClubList from '../ClubList';
import ClubDetail from '../ClubDetail';

export default () => (
	<AppLayout>
		<Router history={browserHistory}>
			<Switch>
				<Route path="/clubs" component={ClubList} />
				<Route path="/club/:id" component={ClubDetail} />
				<Route path="/user/:id" component={null} />
				<Route path="/" component={HomePage} />
			</Switch>
		</Router>
	</AppLayout>
);
