import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../../components/AppButton';

import JoinUsImage from '../../assets/images/joinUs.jpg';

export default () => {
	return (
		<>
			<div className="welcomeSlide">
				<div className="welcomeSlideText appContainer">
					<div className="dashText">Vítejte ve FIT you</div>
					<div className="title">Inspirováno více než 10 tisíc členů</div>
					<div className="subText">
						Ve <b>FIT you</b> věříme, že by si každý měl život užít na plno a zdravý životní styl <br />
						nám umožňuje žít delší, šťastnější a vyváženější život.
					</div>
				</div>
			</div>
			<div className="joinUs">
				<div className="joinWrap">
					<div className="joinTexts">
						<div className="joinTitle">Staň se členem</div>
						<div className="joinText">
							Věříme, že každý z nás by měl žít aktivnější a zdravější život.
							A také věříme, že jednodušší cesta jak tohoto cíle dosáhnout je
							s dobrou komunitou lidí. Zkus svůj nový životní styl ještě dnes!
						</div>
						<div className="joinButton">
							<Link to="/clubs">
								<Button>Najít klub</Button>
							</Link>
						</div>
					</div>
					<div className="joinImage">
						<img src={JoinUsImage} />
					</div>
				</div>
			</div>
		</>
	);
};
