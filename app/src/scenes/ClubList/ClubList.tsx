import React, { Component, ReactNode } from 'react';
import { Card, Row, Col } from 'antd';
import { Link } from 'react-router-dom';

import apiService from '../../services/api';

import Button from '../../components/AppButton';

interface Properties { }

interface State {
	clubs: Array<IClub>;
	loading: boolean;
}

class ClubList extends Component<Properties, State> {

	constructor(props: Properties) {
		super(props);
		this.state = {
			clubs: [],
			loading: true
		};
	}

	componentDidMount(): void {
		this.getClubs();
	}

	getClubs(): void {
		apiService.getClubs().then(clubs => this.setState({ clubs, loading: false }));
	}

	render(): ReactNode {
		if (this.state.loading) { return <Card loading={true} />; }
		return (
			<>
				<div className="clubListHeader">
					<div className="appContainer">Najděte si svůj klub</div>
				</div>
				<div className="appContainer">
					<div className="appClubList">
						{this.state.clubs.map((club: IClub, key) => {
							return (
								<Row key={key} className="clubRow">
									<Col span={6}>
										<div className="clubTitle">{club.name}</div>
										<div className="clubAddress">{club.address}</div>
									</Col>
									<Col span={10} className="clubDescription">{club.description}</Col>
									<Col span={8} className="clubLink">
										<Link to={`/club/${club._id}`}>
											<Button>Navštívit</Button>
										</Link>
									</Col>
								</Row>
							);
						})}
					</div>
				</div>
			</>
		);
	}

}

export default ClubList;
