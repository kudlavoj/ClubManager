//? Core
import React from 'react';
import { Router, Route, Switch } from 'react-router';
//? Utils
import browserHistory from '../../services/history';
//? Components
import AdminLayout from '../../components/AdminLayout';
//? Scenes
import ClubList from '../AdminClubList';
import ClubEdit from '../AdminClubEdit';

export default () => (
	<AdminLayout>
		<Router history={browserHistory}>
			<Switch>
				<Route path="/admin/clubs" component={ClubList} />
				<Route path="/admin/club/:id" component={ClubEdit} />
				<Route path="/admin/events" component={null} />
				<Route path="/admin/event/:id" component={null} />
				<Route path="/admin/users" component={null} />
				<Route path="/admin/user/:id" component={null} />
				<Route path="/admin" component={ClubList} />
			</Switch>
		</Router>
	</AdminLayout>
);
