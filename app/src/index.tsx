//? Core
import React from 'react';
import ReactDOM from 'react-dom';
//? Utils
import * as serviceWorker from './services/serviceWorker';
//? Components
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.register();
