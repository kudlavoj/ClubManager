import React, { ReactNode } from 'react';
import { Layout } from 'antd';

import AppHeader from '../AppHeader';
import AppFooter from '../AppFooter';

const { Content, Header, Footer } = Layout;

interface Properties {
	children: ReactNode;
}

export default ({ children }: Properties) => (
	<Layout className="appLayout">
		<Header className="appLayoutHeader">
			<AppHeader />
		</Header>
		<Content className="appLayoutContent">
			{children}
		</Content>
		<Footer className="appLayoutFooter">
			<AppFooter />
		</Footer>
	</Layout>
);
