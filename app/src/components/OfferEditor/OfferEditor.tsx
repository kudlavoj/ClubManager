import React, { Component, ReactNode } from 'react';
import { Input, Icon, Alert, Card, Button, Modal, notification } from 'antd';

import apiService from '../../services/api';

import InputWrap from '../InputWrap';

interface Properties {
	clubID: RelationID;
	onChange: (offers: Array<RelationID>) => void;
}

interface State {
	loading: boolean;
	error: Error;
	editMode: boolean;
	processing: boolean;
	editOffer: IOffer;
	offers: Array<IOffer>;
}

class OfferEditor extends Component<Properties, State> {

	constructor(props: Properties) {
		super(props);
		this.state = {
			loading: true,
			error: null,
			processing: false,
			editMode: false,
			editOffer: {},
			offers: []
		};
	}

	componentDidMount(): void {
		try {
			apiService.getOffers(this.props.clubID)
				.then(offers => this.setState({ offers, loading: false }));
		} catch (e) {
			this.setState({ loading: false, error: e });
		}
	}

	emitChange = () => {
		this.props.onChange(this.state.offers.map(offer => offer._id));
	}

	// tslint:disable-next-line:no-any
	changeField = ({ key, value }: { key: string, value: any }) => {
		const { editOffer } = this.state;
		editOffer[key] = value;
		this.setState({ editOffer });
	}

	newOffer = () => this.setState({ editMode: true });

	editExistingOffer = (editOffer: IOffer) => this.setState({ editOffer, editMode: true });

	cancelEditing = () => this.setState({ editOffer: {}, editMode: false });

	saveOffer = async () => {
		try {
			const { offers, editOffer } = this.state;
			editOffer.club = this.props.clubID;
			await new Promise(r => this.setState({ processing: true }, r));
			if (editOffer._id) {
				const result = await apiService.updateOffer(editOffer);
				const updatedOffers = offers.map(offer => {
					if (offer._id === result._id) { return result; }
					return offer;
				});
				await new Promise(r => this.setState({ offers: updatedOffers, editOffer: {}, editMode: false }, r));
				notification.success({
					message: 'Úspěch',
					description: 'Nabídka byla úspěšně aktualizována'
				});
			} else {
				const result = await apiService.createOffer(editOffer);
				offers.push(result);
				await new Promise(r => this.setState({ offers, editOffer: {}, editMode: false }, r));
				notification.success({
					message: 'Úspěch',
					description: 'Nabídka byla úspěšně vytvořena'
				});
			}
		} catch (e) {
			notification.error({
				message: e.name,
				description: e.message
			});
		} finally {
			this.setState({ processing: false });
			this.emitChange();
		}
	}

	deleteOffer = async () => {
		try {
			const { offers, editOffer } = this.state;
			await new Promise(r => this.setState({ processing: true }, r));
			if (editOffer._id) {
				Modal.confirm({
					title: <span>Chcete smazat nabídku <b>{editOffer.name}</b>?</span>,
					content: 'Tato akce je nezvratná',
					onOk: async () => {
						const ok = await apiService.deleteOffer(editOffer._id);
						if (ok) {
							const updatedOffers = offers.filter(offer => offer._id !== editOffer._id);
							await new Promise(r => this.setState({ offers: updatedOffers, editOffer: {}, editMode: false }, r));
							notification.success({
								message: 'Úspěch',
								description: 'Nabídka byla úspěšně smazána'
							});
						} else {
							throw new Error('Chyba při mazání nabídky');
						}
					}
				});
			} else {
				await new Promise(r => this.setState({ editOffer: {}, editMode: false }));
			}
		} catch (e) {
			notification.error({
				message: e.name,
				description: e.message
			});
		} finally {
			this.setState({ processing: false });
			this.emitChange();
		}
	}

	render(): ReactNode {
		const { loading, error, offers, editMode, editOffer, processing } = this.state;
		if (loading) { return <Icon type="loading" />; }
		if (error) { return <Alert type="error" message={error.name} description={error.message} />; }
		const BreakLine = () => <div style={{ height: 1, background: 'rgba(0,0,0,0.1)', margin: '5px 0' }} />;
		return (
			<div>
				{offers.map((offer, key) => {
					if (editMode && editOffer._id === offer._id) {
						return (
							<Card key={key} size="small" style={{ marginBottom: 10 }}>
								<InputWrap title="Název" required={true}>
									<Input
										value={editOffer.name}
										onChange={({ target: { value } }) => this.changeField({ key: 'name', value })}
									/>
								</InputWrap>
								<InputWrap title="Popis" required={true}>
									<Input.TextArea
										autosize={{ minRows: 3 }}
										value={editOffer.description}
										style={{ resize: 'none' }}
										onChange={({ target: { value } }) => this.changeField({ key: 'description', value })}
									/>
								</InputWrap>
								<InputWrap title="Max lekcí (prázdné pokud neomezeně)">
									<Input
										value={editOffer.maxClasses || ''}
										onChange={({ target: { value } }) => this.changeField({
											key: 'maxClasses',
											value: Number(value.replace(/[^0-9]/g, '')) || ''
										})}
									/>
								</InputWrap>
								<InputWrap title="Cena (Kč)" required={true}>
									<Input
										value={editOffer.price || ''}
										onChange={({ target: { value } }) => this.changeField({
											key: 'price',
											value: Number(value.replace(/[^0-9]/g, '')) || ''
										})}
									/>
								</InputWrap>
								<div style={{ textAlign: 'right' }}>
									<Button
										loading={processing}
										onClick={this.cancelEditing}
									>
										Zrušit
									</Button>
									<Button
										loading={processing}
										type="danger"
										ghost={true}
										style={{ marginLeft: 5 }}
										onClick={this.deleteOffer}
									>
										Smazat
									</Button>
									<Button
										loading={processing}
										type="primary"
										style={{ marginLeft: 5 }}
										onClick={this.saveOffer}
									>
										Uložit nabídku
									</Button>
								</div>
							</Card>
						);
					}
					return (
						<Card key={key} size="small" style={{ marginBottom: 10 }}>
							<InputWrap title="Název" space={0}>
								<div style={{ fontWeight: 500 }}>{offer.name}</div>
							</InputWrap>
							<BreakLine />
							<InputWrap title="Popis" space={0}>
								<div style={{ fontWeight: 500 }}>{offer.description}</div>
							</InputWrap>
							<BreakLine />
							<InputWrap title="Počet lekcí" space={0}>
								<div style={{ fontWeight: 500 }}>{offer.maxClasses || 'Neomezeně'}</div>
							</InputWrap>
							<BreakLine />
							<InputWrap title="Cena" space={0}>
								<div style={{ fontWeight: 500 }}>{offer.price} Kč</div>
							</InputWrap>
							<div style={{ textAlign: 'right' }}>
								<Button onClick={() => this.editExistingOffer(offer)}>Upravit</Button>
							</div>
						</Card>
					);
				})}
				{editMode && !editOffer._id && (
					<Card size="small">
						<InputWrap title="Název" required={true}>
							<Input
								value={editOffer.name}
								onChange={({ target: { value } }) => this.changeField({ key: 'name', value })}
							/>
						</InputWrap>
						<InputWrap title="Popis" required={true}>
							<Input.TextArea
								autosize={{ minRows: 3 }}
								value={editOffer.description}
								style={{ resize: 'none' }}
								onChange={({ target: { value } }) => this.changeField({ key: 'description', value })}
							/>
						</InputWrap>
						<InputWrap title="Max lekcí (prázdné pokud neomezeně)">
							<Input
								value={editOffer.maxClasses || ''}
								onChange={({ target: { value } }) => this.changeField({
									key: 'maxClasses',
									value: Number(value.replace(/[^0-9]/g, '')) || ''
								})}
							/>
						</InputWrap>
						<InputWrap title="Cena (Kč)" required={true}>
							<Input
								value={editOffer.price || ''}
								onChange={({ target: { value } }) => this.changeField({
									key: 'price',
									value: Number(value.replace(/[^0-9]/g, '')) || ''
								})}
							/>
						</InputWrap>
						<div style={{ textAlign: 'right' }}>
							<Button
								loading={processing}
								type="danger"
								ghost={true}
								onClick={this.deleteOffer}
							>
								Smazat
							</Button>
							<Button
								loading={processing}
								type="primary"
								style={{ marginLeft: 5 }}
								onClick={this.saveOffer}
							>
								Uložit nabídku
							</Button>
						</div>
					</Card>
				)}
				{!editMode && (
					<div>
						<Button onClick={this.newOffer} style={{ width: '100%' }} type="primary">Vytvořit novou nabídku</Button>
					</div>
				)}
			</div>
		);
	}

}

export default OfferEditor;
