//? Core
import React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';

export default ({ location: { pathname } }: WithRouterProps) => (
	<Menu
		mode="inline"
		style={{ border: 'none' }}
		defaultSelectedKeys={[pathname, pathname.split('/').splice(0, 3).join('/')]}
	>
		<Menu.Item key="/admin/clubs">
			<Link to="/admin/clubs">Kluby</Link>
		</Menu.Item>
		<Menu.Item key="/admin/events">
			<Link to="/admin/events">Události</Link>
		</Menu.Item>
		<Menu.Item key="/admin/users">
			<Link to="/admin/users">Uživatelé</Link>
		</Menu.Item>
	</Menu>
);
