import React from 'react';
import { Row, Col, Icon } from 'antd';

export default () => (
	<div className="appFooter appContainer">
		<Row>
			<Col span={12}>
				<div className="footerTitle">Hnutí</div>
				<div className="footerText">Pro užití si cesty</div>
				<div className="footerTitle">Místo</div>
				<div className="footerText">Pro vytěžení nejvíce ze života</div>
				<div className="footerTitle">Komunita</div>
				<div className="footerText">pro snadnější růst</div>
				<div className="footerTitle">Životní styl</div>
				<div className="footerText">K dosažení vašich tužeb</div>
			</Col>
			<Col span={12}>
				<div className="contactTitle">Kontakt</div>
				<div className="contactRow"><Icon type="home" /> Thákurova 9, 160 00 Praha 6</div>
				<div className="contactRow"><Icon type="mail" /> clubmanagersi12@gmail.com</div>
			</Col>
		</Row>
	</div>
);
