import React, { ReactNode } from 'react';
import { Layout } from 'antd';
import AdminMenu from '../AdminMenu';
const { Content, Sider, Header } = Layout;

interface Properties {
	children: ReactNode;
}

export default ({ children }: Properties) => (
	<Layout style={{ background: '#fff', minHeight: '100vh' }}>
		<Header
			style={{
				background: '#fff',
				borderBottom: '1px solid #ebedf0',
				boxShadow: '0 2px 8px #f0f1f2',
				fontSize: 16,
				zIndex: 10
			}}
		>
			<span>Administrace</span>
		</Header>
		<Layout style={{ background: '#fff', paddingTop: 30 }}>
			<Sider width={200} style={{ background: '#fff', borderRight: '1px solid #ebedf0' }}>
				<AdminMenu />
			</Sider>
			<Layout>
				<Content style={{ background: '#fff', padding: '0 30px' }}>{children}</Content>
			</Layout>
		</Layout>
	</Layout>
);
