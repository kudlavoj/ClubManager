import React, { Component, ReactNode } from 'react';
import { Tag, Input, Tooltip, Icon, Alert } from 'antd';

import apiService from '../../services/api';

interface Properties {
	clubID: RelationID;
	onChange: (coaches: Array<RelationID>) => void;
}

interface State {
	loading: boolean;
	error: Error;
	inputValue: string;
	inputVisible: boolean;
	coaches: Array<ICoach>;
}

class CoachPicker extends Component<Properties, State> {

	private input: Input;

	constructor(props: Properties) {
		super(props);
		this.state = {
			loading: true,
			error: null,
			inputValue: '',
			inputVisible: false,
			coaches: []
		};
	}

	componentDidMount(): void {
		try {
			apiService.getCoaches(this.props.clubID)
				.then(coaches => this.setState({ coaches, loading: false }));
		} catch (e) {
			this.setState({ loading: false, error: e });
		}
	}

	emitChange = () => {
		this.props.onChange(this.state.coaches.map(coach => coach._id));
	}

	handleClose = async (coachToDelete: ICoach) => {
		const ok = await apiService.deleteCoach(coachToDelete._id);
		if (ok) {
			const coaches = this.state.coaches.filter(coach => coach.name !== coachToDelete.name);
			this.setState({ coaches }, this.emitChange);
		}
	}

	showInput = () => this.setState({ inputVisible: true }, () => this.input.focus());

	handleInputChange = ({ target: { value: inputValue } }: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({ inputValue });
	}

	handleInputConfirm = async () => {
		const { inputValue } = this.state;
		let { coaches } = this.state;
		if (inputValue && !coaches.find(coach => coach.name === inputValue)) {
			const newCoach = await apiService.createCoach({ name: inputValue, club: this.props.clubID });
			coaches.push(newCoach);
			this.emitChange();
		}
		this.setState({
			coaches,
			inputVisible: false,
			inputValue: '',
		});
	}

	saveInputRef = (input: Input) => this.input = input;

	render(): ReactNode {
		const { loading, error, coaches, inputVisible, inputValue } = this.state;
		if (loading) { return <Icon type="loading" />; }
		if (error) { return <Alert type="error" message={error.name} description={error.message} />; }
		return (
			<div>
				{coaches.map((coach, key) => {
					const isLongTag = coach.name.length > 20;
					const tagElem = (
						<Tag key={key} closable={true} visible={true} onClose={() => this.handleClose(coach)}>
							{isLongTag ? `${coach.name.slice(0, 20)}...` : coach.name}
						</Tag>
					);
					return isLongTag ? <Tooltip title={coach.name} key={key}>{tagElem}</Tooltip> : tagElem;
				})}

				{inputVisible && (
					<Input
						ref={this.saveInputRef}
						type="text"
						size="small"
						style={{ width: 78 }}
						value={inputValue}
						onChange={this.handleInputChange}
						onBlur={this.handleInputConfirm}
						onPressEnter={this.handleInputConfirm}
					/>
				)}

				{!inputVisible && (
					<Tag
						onClick={this.showInput}
						style={{ background: '#fff', borderStyle: 'dashed' }}
					>
						<Icon type="plus" /> Nový trenér
					</Tag>
				)}
			</div>
		);
	}

}

export default CoachPicker;
