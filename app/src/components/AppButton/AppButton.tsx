import React, { ReactNode } from 'react';

interface Properties {
	children: ReactNode;
	onClick?: () => void;
}

export default ({ children, onClick }: Properties) => (
	<div className="appButton">
		<button onClick={onClick}>{children}</button>
	</div>
);
