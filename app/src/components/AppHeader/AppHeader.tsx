import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
	<div className="appHeader">
		<Link to="/">
			<div className="appLogo">
				<span className="fit">fit</span>
				<span className="you">you</span>
			</div>
		</Link>
	</div>
);
