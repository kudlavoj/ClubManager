import React, { ReactNode } from 'react';

interface Properties {
	title: string;
	children: ReactNode;
	required?: boolean;
	space?: number;
}

export default ({ children, title, required, space }: Properties) => (
	<div style={{ marginBottom: space !== undefined ? space : 10 }}>
		<div>{title} {required && <span style={{ color: 'red' }}>*</span>}</div>
		<div>{children}</div>
	</div>
);
