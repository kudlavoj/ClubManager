import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, IClub } from '@source/interfaces';

class ClubController extends DefaultController {

	public getClubs = (): Promise<Array<IClub>> => {
		return new Promise((resolve, reject) => {
			models.ClubModel.find((err: Error, res: Array<IClub>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getClub = ({ id }): Promise<IClub> => {
		return new Promise((resolve, reject) => {
			models.ClubModel.findById(id, (err: Error, res: IClub) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
  }
  
	public createClub = ({ data }: MutationBody<IClub>): Promise<IClub> => {
		return new Promise((resolve, reject) => {
			const { name, description, address } = data;
			const club = new models.ClubModel({ name, description, address, events: [], coaches: [], lections: [], offers: [] });
			club.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public updateClub = ({ data, where: { id } }: MutationBody<IClub>): Promise<IClub> => {
		return new Promise((resolve, reject) => {
			models.ClubModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public deleteClub = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.ClubModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			});
		});
	}

};

export default ClubController;
