import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, IEvent } from '@source/interfaces';

class EventController extends DefaultController {

	public getEvents = (): Promise<Array<IEvent>> => {
		return new Promise((resolve, reject) => {
			models.EventModel.find((err: Error, res: Array<IEvent>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getEvent = ({ id }): Promise<IEvent> => {
		return new Promise((resolve, reject) => {
			models.EventModel.findById(id, (err: Error, res: IEvent) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
  }

	public createEvent = ({ data }: MutationBody<IEvent>): Promise<IEvent> => {
		return new Promise((resolve, reject) => {
			const { name, description, start, end, club, capacity } = data;
			const clubEvent = new models.EventModel({ name, description, start, end, club, capacity, participants: [] });
			clubEvent.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public updateEvent = ({ data, where: { id } }: MutationBody<IEvent>): Promise<IEvent> => {
		return new Promise((resolve, reject) => {
			models.EventModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public deleteEvent = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.EventModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			});
		});
	}

};

export default EventController;
