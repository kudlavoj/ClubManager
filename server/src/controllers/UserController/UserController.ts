import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, IUser } from '@source/interfaces';

class UserController extends DefaultController {

  public getUsers = (): Promise<Array<IUser>> => {
		return new Promise((resolve, reject) => {
			models.UserModel.find((err: Error, res: Array<IUser>) => {
				if (err) return reject(err);
				return resolve(res);
			});
    });
	}

	public getUser = ({ id }): Promise<IUser> => {
		return new Promise((resolve, reject) => {
			models.UserModel.findById(id, (err: Error, res: IUser) => {
				if (err) return reject(err);
        return resolve(res);
			})
		});
	}

	public createUser = ({ data }: MutationBody<IUser>): Promise<IUser> => {
		return new Promise((resolve, reject) => {
      const { name, pass, email, phone, address } = data;
      const user = new models.UserModel({
        name,
        pass,
        email,
        phone,
        address,
        classInstances: [],
        events: [],
        memberships: []
      });
			user.save((err, res) => {
				if (err) return reject(err);
        return resolve(res);
			});
		})
	}

	public updateUser = ({ data, where: { id } }: MutationBody<IUser>): Promise<IUser> => {
		return new Promise((resolve, reject) => {
			models.UserModel.findByIdAndUpdate(id, data, (err, res) => {
				if (err) return reject(err);
        return resolve(res);
			});
		});
	}

	public deleteUser = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.UserModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			})
		})
	}

};

export default UserController;
