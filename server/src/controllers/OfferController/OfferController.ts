import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, IOffer } from '@source/interfaces';

class OfferController extends DefaultController {

	public getOffers = (): Promise<Array<IOffer>> => {
		return new Promise((resolve, reject) => {
			models.OfferModel.find((err: Error, res: Array<IOffer>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getOffer = ({ id }): Promise<IOffer> => {
		return new Promise((resolve, reject) => {
			models.OfferModel.findById(id, (err: Error, res: IOffer) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public createOffer = ({ data }: MutationBody<IOffer>): Promise<IOffer> => {
		return new Promise((resolve, reject) => {
			const { name, description, price, maxClasses, club } = data;
			const offer = new models.OfferModel({ name, description, price, maxClasses, club });
			offer.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public updateOffer = ({ data, where: { id } }: MutationBody<IOffer>): Promise<IOffer> => {
		return new Promise((resolve, reject) => {
			models.OfferModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public deleteOffer = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.OfferModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			});
		});
	}

};

export default OfferController;
