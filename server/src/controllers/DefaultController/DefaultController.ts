interface Validator {
  type?: 'boolean' | 'number' | 'object' | 'string';
  required?: boolean;
  max?: number;
  min?: number;
  maxLength?: number;
  minLength?: number;
}

interface ValidateInput {
  value: any;
  validator: Validator;
}

class DefaultController {

  private isValid({ value, validator }: ValidateInput): boolean {
    if (validator.required && !value) return false;
    if (validator.type && typeof value === validator.type) return false;
    if (validator.min && Number(value) < validator.min) return false;
    if (validator.max && Number(value) > validator.max) return false;
    if (validator.minLength && String(value).length < validator.minLength) return false;
    if (validator.maxLength && String(value).length > validator.maxLength) return false;
    return true;
  }

  public validate(input: ValidateInput | Array<ValidateInput>): boolean {
    if (Array.isArray(input))
      return input.every(value => this.isValid(value));
    return this.isValid(input);
  }

}

export default DefaultController;
