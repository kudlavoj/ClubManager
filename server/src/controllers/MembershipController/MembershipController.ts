import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, IMembership } from '@source/interfaces';

class MembershipController extends DefaultController {

	public getMemberships = (): Promise<Array<IMembership>> => {
		return new Promise((resolve, reject) => {
			models.MembershipModel.find((err: Error, res: Array<IMembership>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getMembership = ({ id }): Promise<IMembership> => {
		return new Promise((resolve, reject) => {
			models.MembershipModel.findById(id, (err: Error, res: IMembership) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public createMembership = ({ data }: MutationBody<IMembership>): Promise<IMembership> => {
		return new Promise((resolve, reject) => {
			const { active, club, expire, offer, user } = data;
			const membership = new models.MembershipModel({ active, club, expire, offer, user });
			membership.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public updateMembership = ({ data, where: { id } }: MutationBody<IMembership>): Promise<IMembership> => {
		return new Promise((resolve, reject) => {
			models.MembershipModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public deleteMembership = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.MembershipModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			})
		});
	}

};

export default MembershipController;
