import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, IClubClass, IClassInstance } from '@source/interfaces';

class ClassController extends DefaultController {

	public getClasses = (): Promise<Array<IClubClass>> => {
		return new Promise((resolve, reject) => {
			models.ClubClassModel.find((err: Error, res: Array<IClubClass>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getClass = ({ id }): Promise<IClubClass> => {
		return new Promise((resolve, reject) => {
			models.ClubClassModel.findById(id, (err: Error, res: IClubClass) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getClassInstances = (): Promise<Array<IClassInstance>> => {
		return new Promise((resolve, reject) => {
			models.ClassInstanceModel.find((err: Error, res: Array<IClassInstance>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getClassInstance = ({ id }): Promise<IClassInstance> => {
		return new Promise((resolve, reject) => {
			models.ClassInstanceModel.findById(id, (err: Error, res: IClassInstance) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

  public createClass = ({ data }: MutationBody<IClubClass>): Promise<IClubClass> => {
		return new Promise((resolve, reject) => {
			const { active, capacity, type, coach, club } = data;
			const clubClass = new models.ClubClassModel({ active, capacity, type, coach, club, instances: [] });
			clubClass.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public updateClass = ({ data, where: { id } }: MutationBody<IClubClass>): Promise<IClubClass> => {
		return new Promise((resolve, reject) => {
			models.ClubClassModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public deleteClass = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.ClubClassModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			});
		});
	}

	public createClassInstance = ({ data }: MutationBody<IClassInstance>): Promise<IClassInstance> => {
		return new Promise((resolve, reject) => {
			const { start, end, clubClass } = data;
			const clubClassInstance = new models.ClassInstanceModel({ start, end, clubClass });
			clubClassInstance.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public updateClassInstance = ({ data, where: { id } }: MutationBody<IClassInstance>): Promise<IClassInstance> => {
		return new Promise((resolve, reject) => {
			models.ClassInstanceModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		});
	}

	public deleteClassInstance = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.ClassInstanceModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			});
		});
	}

};

export default ClassController;
