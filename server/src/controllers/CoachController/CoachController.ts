import * as models from '@source/models';
import DefaultController from '../DefaultController';
import { MutationBody, ICoach } from '@source/interfaces';

class CoachController extends DefaultController {

	public getCoaches = (): Promise<Array<ICoach>> => {
		return new Promise((resolve, reject) => {
			models.CoachModel.find((err: Error, res: Array<ICoach>) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public getCoach = ({ id }): Promise<ICoach> => {
		return new Promise((resolve, reject) => {
			models.CoachModel.findById(id, (err: Error, res: ICoach) => {
				if (err) return reject(err);
				return resolve(res);
			});
		});
	}

	public createCoach = ({ data }: MutationBody<ICoach>): Promise<ICoach> => {
		return new Promise((resolve, reject) => {
			const { name, club } = data;
			const coach = new models.CoachModel({ name, club, classes: [] });
			coach.save((err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		})
	}

	public updateCoach = ({ data, where: { id } }: MutationBody<ICoach>): Promise<ICoach> => {
		return new Promise((resolve, reject) => {
			models.CoachModel.findByIdAndUpdate(id, data, (err, result) => {
				if (err) return reject(err);
				return resolve(result);
			});
		})
	}

	public deleteCoach = ({ where: { id } }: MutationBody<void>): Promise<boolean> => {
		return new Promise((resolve, reject) => {
			models.CoachModel.findByIdAndDelete(id, (err) => {
				if (err) return reject(err);
				return resolve(true);
			});
		});
	}

};

export default CoachController;
