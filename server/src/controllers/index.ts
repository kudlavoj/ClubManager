import classController from './ClassController';
import clubController from './ClubController';
import coachController from './CoachController';
import eventController from './EventController';
import membershipController from './MembershipController';
import offerController from './OfferController';
import userController from './UserController';

export {
  classController,
  clubController,
  coachController,
  eventController,
  membershipController,
  offerController,
  userController
};
