import { Document, Schema, Model, model} from 'mongoose';
import { IOffer } from '@source/interfaces';

export interface IOfferModel extends IOffer, Document {
  createdAt: Date;
}

export const OfferSchema: Schema = new Schema({
  name: String,
	description: String,
	price: Number,
	maxClasses: Number,	
	club: String,
  createdAt: Date,
});

OfferSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const OfferModel: Model<IOfferModel> = model<IOfferModel>('Offer', OfferSchema);
