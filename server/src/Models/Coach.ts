import { Document, Schema, Model, model} from 'mongoose';
import { ICoach } from '@source/interfaces';

export interface ICoachModel extends ICoach, Document {
  createdAt: Date;
}

export const CoachSchema: Schema = new Schema({
	name: String,
	classes: Array,
  club: String,
  createdAt: Date,
});

CoachSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const CoachModel: Model<ICoachModel> = model<ICoachModel>('Coach', CoachSchema);
