import { Document, Schema, Model, model} from 'mongoose';
import { IClassInstance } from '@source/interfaces';

export interface IClassInstanceModel extends IClassInstance, Document {
  createdAt: Date;
}

export const ClassInstanceSchema: Schema = new Schema({
  start: String,
	end: String,
	class: String,
  createdAt: Date,
});

ClassInstanceSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const ClassInstanceModel: Model<IClassInstanceModel> = model<IClassInstanceModel>('ClassInstance', ClassInstanceSchema);
