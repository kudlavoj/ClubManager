import { Document, Schema, Model, model} from 'mongoose';
import { IClub } from '@source/interfaces';

export interface IClubModel extends IClub, Document {
  createdAt: Date;
}

export const ClubSchema: Schema = new Schema({
	name: String,
	description: String,
	address: String,
	coaches: Array,
	classes: Array,
	offers: Array,
	events: Array,
  createdAt: Date,
});

ClubSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const ClubModel: Model<IClubModel> = model<IClubModel>('Club', ClubSchema);
