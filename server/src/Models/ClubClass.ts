import { Document, Schema, Model, model} from 'mongoose';
import { IClubClass } from '@source/interfaces';

export interface IClubClassModel extends IClubClass, Document {
  createdAt: Date;
}

export const ClubClassSchema: Schema = new Schema({
	active: Boolean,
	capacity: Number,
	type: String,
	coach: String,
	club: String,
	instances: Array,
  createdAt: Date,
});

ClubClassSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const ClubClassModel: Model<IClubClassModel> = model<IClubClassModel>('Class', ClubClassSchema);
