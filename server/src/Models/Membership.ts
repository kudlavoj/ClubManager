import { Document, Schema, Model, model} from 'mongoose';
import { IMembership } from '@source/interfaces';

export interface IMembershipModel extends IMembership, Document {
  createdAt: Date;
}

export const MembershipSchema: Schema = new Schema({
	active: Boolean,
	expire: Date,
	user: String,
	offer: String,
	club: String,
  createdAt: Date,
});

MembershipSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const MembershipModel: Model<IMembershipModel> = model<IMembershipModel>('Membership', MembershipSchema);
