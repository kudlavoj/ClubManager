import { Document, Schema, Model, model} from 'mongoose';
import { IUser } from '@source/interfaces';

export interface IUserModel extends IUser, Document {
  createdAt: Date;
}

export const UserSchema: Schema = new Schema({
  name: String,
	email: String,
	phone: String,
	address: String,
	pass: String,
	events: Array,
	classInstances: Array,
	memberships: Array,
  createdAt: Date,
});

UserSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const UserModel: Model<IUserModel> = model<IUserModel>('User', UserSchema);
