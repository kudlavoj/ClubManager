import { Document, Schema, Model, model} from 'mongoose';
import { IEvent } from '@source/interfaces';

export interface IEventModel extends IEvent, Document {
  createdAt: Date;
}

export const EventSchema: Schema = new Schema({
	name: String,
	description: String,
	capacity: Number,
	start: Date,
	end: Date,
	participants: Array,
	club: String,
  createdAt: Date,
});

EventSchema.pre('save', (next) => {
  if (!this.createdAt)
    this.createdAt = new Date();
  next();
});

export const EventModel: Model<IEventModel> = model<IEventModel>('Event', EventSchema);
