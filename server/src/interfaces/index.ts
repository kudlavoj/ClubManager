export interface MutationBody<T> {
	where?: { id: string; };
	data?: T;
}

type ID = string;

export interface IUser {
	name: string;
	email: string;
	phone: string;
	address: string;
	pass: string;
	// Relations
	events: Array<ID>;
	classInstances: Array<ID>;
	memberships: Array<ID>;
}

export interface ICoach {
	name: string;
	// Relations
	classes: Array<ID>;
	club: ID;
}

export interface IClub {
	name: string;
	description: string;
	address: string;
	// Relations
	coaches: Array<ID>;
	classes: Array<ID>;
	offers: Array<ID>;
	events: Array<ID>;
}

export interface IOffer {
	name: string;
	description: string;
	price: number;
	maxClasses: number;	
	// Relations
	club: ID;
}

export interface IMembership {
	active: boolean;
	expire: Date;
	// Relations
	user: ID;
	offer: ID;
	club: ID;
}

export interface IClubClass {
	active: boolean;
	capacity: number;
	type: string;
	coach: ID;
	club: ID;
	// Relations
	instances: Array<ID>;
}

export interface IClassInstance {
	start: string;
	end: string;
	// Relations
	clubClass: ID;
}

export interface IEvent {
	name: string;
	description: string;
	capacity: number;
	start: Date;
	end: Date;
	// Relations
	participants: Array<ID>;
	club: ID;
}