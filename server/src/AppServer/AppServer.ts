import * as express from 'express';
import * as cors from 'cors';
import chalk from 'chalk';

import { ApiRouter, AppHostingRouter } from '@source/routers';

class AppServer {

	private port: Number;
	private app: express.Application;

	private apiRouter: ApiRouter;
	private appHostingRouter: AppHostingRouter;

	constructor(port: Number) {
		this.port = port;
		this.app = express();
		this.apiRouter = new ApiRouter();
		this.appHostingRouter = new AppHostingRouter();
	}

	private async runServer(): Promise<void> {
		await new Promise(r => this.app.listen(this.port, r));
		console.log(`Server is running on port ${chalk.blue(this.port.toString())}`);
	}

	private async applyMiddlewares(): Promise<void> {
		this.app.use(cors());
	}

	private async applyRouters(): Promise<void> {
		this.apiRouter.applyMiddlewares();
		this.apiRouter.defineRoutes();
		this.app.use('/api', this.apiRouter.Router);
		console.log(`${chalk.green('Api')} is running on route ${chalk.blue('/api')}`);

		this.appHostingRouter.defineRoutes();
		this.app.use('/', this.appHostingRouter.Router);
		console.log(`${chalk.green('App')} is hosted`);
	}
	
	public async start(): Promise<void> {
		await this.runServer();
		await this.applyMiddlewares();
		await this.applyRouters();
	}

}

export default AppServer;
