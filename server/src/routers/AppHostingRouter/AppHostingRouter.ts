import { Router, Request, Response } from 'express';
import * as express from 'express';
import * as path from 'path';

import DefaultRouter from '../DefaultRouter';

class AppHostingRouter extends DefaultRouter {

  constructor() {
    super(Router());
  }

  public defineRoutes(): void {
    this.router.use(express.static(path.join(__dirname, 'build')));
    this.router.get('*', (req: Request, res: Response) => res.sendFile(path.join(__dirname, 'build', 'index.html')));
  }

}

export default AppHostingRouter;
