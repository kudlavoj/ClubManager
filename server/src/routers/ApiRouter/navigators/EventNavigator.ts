import { Router, Request, Response } from 'express';
import { eventController } from '@source/controllers';

class EventNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/events', async (req: Request, res: Response) => {
      try {
        res.send(await eventController.getEvents());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/event/:id', async (req: Request, res: Response) => {
      try {
        res.send(await eventController.getEvent(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createEvent', async (req: Request, res: Response) => {
      try {
        res.send(await eventController.createEvent(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateEvent', async (req: Request, res: Response) => {
      try {
        res.send(await eventController.updateEvent(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteEvent', async (req: Request, res: Response) => {
      try {
        res.send(await eventController.deleteEvent(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default EventNavigator;
