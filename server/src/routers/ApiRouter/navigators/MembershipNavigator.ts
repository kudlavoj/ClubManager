import { Router, Request, Response } from 'express';
import { membershipController } from '@source/controllers';

class MembershipNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/memberships', async (req: Request, res: Response) => {
      try {
        res.send(await membershipController.getMemberships());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/membership/:id', async (req: Request, res: Response) => {
      try {
        res.send(await membershipController.getMembership(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createMembership', async (req: Request, res: Response) => {
      try {
        res.send(await membershipController.createMembership(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateMembership', async (req: Request, res: Response) => {
      try {
        res.send(await membershipController.updateMembership(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteMembership', async (req: Request, res: Response) => {
      try {
        res.send(await membershipController.deleteMembership(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default MembershipNavigator;
