import { Router, Request, Response } from 'express';
import { offerController } from '@source/controllers';

class OfferNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/offers', async (req: Request, res: Response) => {
      try {
        res.send(await offerController.getOffers());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/offer/:id', async (req: Request, res: Response) => {
      try {
        res.send(await offerController.getOffer(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createOffer', async (req: Request, res: Response) => {
      try {
        res.send(await offerController.createOffer(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateOffer', async (req: Request, res: Response) => {
      try {
        res.send(await offerController.updateOffer(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteOffer', async (req: Request, res: Response) => {
      try {
        res.send(await offerController.deleteOffer(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default OfferNavigator;
