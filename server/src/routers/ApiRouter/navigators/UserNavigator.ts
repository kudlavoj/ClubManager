import { Router, Request, Response } from 'express';
import { userController } from '@source/controllers';

class UserNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/users', async (req: Request, res: Response) => {
      try {
        res.send(await userController.getUsers());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/user/:id', async (req: Request, res: Response) => {
      try {
        res.send(await userController.getUser(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createUser', async (req: Request, res: Response) => {
      try {
        res.send(await userController.createUser(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateUser', async (req: Request, res: Response) => {
      try {
        res.send(await userController.updateUser(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteUser', async (req: Request, res: Response) => {
      try {
        res.send(await userController.deleteUser(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default UserNavigator;
