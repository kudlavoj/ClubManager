import { Router, Request, Response } from 'express';
import { clubController } from '@source/controllers';

class ClubNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/clubs', async (req: Request, res: Response) => {
      try {
        res.send(await clubController.getClubs());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/club/:id', async (req: Request, res: Response) => {
      try {
        res.send(await clubController.getClub(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createClub', async (req: Request, res: Response) => {
      try {
        res.send(await clubController.createClub(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateClub', async (req: Request, res: Response) => {
      try {
        res.send(await clubController.updateClub(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteClub', async (req: Request, res: Response) => {
      try {
        res.send(await clubController.deleteClub(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default ClubNavigator;
