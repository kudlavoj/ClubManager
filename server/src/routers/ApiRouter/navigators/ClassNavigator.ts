import { Router, Request, Response } from 'express';
import { classController } from '@source/controllers';

class ClassNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/classes', async (req: Request, res: Response) => {
      try {
        res.send(await classController.getClasses());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/class/:id', async (req: Request, res: Response) => {
      try {
        res.send(await classController.getClass(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/classInstances', async (req: Request, res: Response) => {
      try {
        res.send(await classController.getClassInstances());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/classInstance/:id', async (req: Request, res: Response) => {
      try {
        res.send(await classController.getClassInstance(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createClass', async (req: Request, res: Response) => {
      try {
        res.send(await classController.createClass(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateClass', async (req: Request, res: Response) => {
      try {
        res.send(await classController.updateClass(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteClass', async (req: Request, res: Response) => {
      try {
        res.send(await classController.deleteClass(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createClassInstance', async (req: Request, res: Response) => {
      try {
        res.send(await classController.createClassInstance(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateClassInstance', async (req: Request, res: Response) => {
      try {
        res.send(await classController.updateClassInstance(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteClassInstance', async (req: Request, res: Response) => {
      try {
        res.send(await classController.deleteClassInstance(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default ClassNavigator;
