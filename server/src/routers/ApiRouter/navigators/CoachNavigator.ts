import { Router, Request, Response } from 'express';
import { coachController } from '@source/controllers';

class CoachNavigator {

  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  public defineRoutes(): void {
    this.router.get('/coaches', async (req: Request, res: Response) => {
      try {
        res.send(await coachController.getCoaches());
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.get('/coach/:id', async (req: Request, res: Response) => {
      try {
        res.send(await coachController.getCoach(req.params));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/createCoach', async (req: Request, res: Response) => {
      try {
        res.send(await coachController.createCoach(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/updateCoach', async (req: Request, res: Response) => {
      try {
        res.send(await coachController.updateCoach(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });

    this.router.post('/deleteCoach', async (req: Request, res: Response) => {
      try {
        res.send(await coachController.deleteCoach(req.body));
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    });
  }

}

export default CoachNavigator;
