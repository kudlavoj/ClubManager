import { Router, Request, Response } from 'express';
import dataManager from '@source/DataManager';
import * as bodyParser from 'body-parser';

import DefaultRouter from '../DefaultRouter';
import {
	ClassNavigator,
	ClubNavigator,
	CoachNavigator,
	EventNavigator,
	MembershipNavigator,
	OfferNavigator,
	UserNavigator
} from './navigators';

interface IApiRequest {
	route: string;
	func: Function;
};

class ApiRouter extends DefaultRouter {

	private classNavigator: ClassNavigator;
	private clubNavigator: ClubNavigator;
	private coachNavigator: CoachNavigator;
	private eventNavigator: EventNavigator;
	private membershipNavigator: MembershipNavigator;
	private offerNavigator: OfferNavigator;
	private userNavigator: UserNavigator;

	constructor() {
		super(Router());
		this.classNavigator = new ClassNavigator(this.router);
		this.clubNavigator = new ClubNavigator(this.router);
		this.coachNavigator = new CoachNavigator(this.router);
		this.eventNavigator = new EventNavigator(this.router);
		this.membershipNavigator = new MembershipNavigator(this.router);
		this.offerNavigator = new OfferNavigator(this.router);
		this.userNavigator = new UserNavigator(this.router);
	}

	public defineRoutes(): void {
		this.router.get('/', (_, res: Response) => res.send('Yep, API is working. :)'));

		this.classNavigator.defineRoutes();
		this.clubNavigator.defineRoutes();
		this.coachNavigator.defineRoutes();
		this.eventNavigator.defineRoutes();
		this.membershipNavigator.defineRoutes();
		this.offerNavigator.defineRoutes();
		this.userNavigator.defineRoutes();
	}

	public applyMiddlewares(): void {
		this.router.use(bodyParser.urlencoded({ extended: true }));
		this.router.use(bodyParser.json());
	}

}

export default ApiRouter;
