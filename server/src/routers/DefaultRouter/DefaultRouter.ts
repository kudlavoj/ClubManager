import { Router } from 'express';

class DefaultRouter {

  public router: Router;
  
  constructor(router: Router) {
    this.router = router;
  }

  public get Router(): Router {
    return this.router;
  }

}

export default DefaultRouter;
