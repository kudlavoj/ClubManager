import ApiRouter from './ApiRouter';
import AppHostingRouter from './AppHostingRouter';

export {
  ApiRouter,
  AppHostingRouter
};
