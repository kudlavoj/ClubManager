import * as dotenv from 'dotenv';
import chalk from 'chalk';

class EnviromentLoader {

  static async loadEnviroment(): Promise<void> {
    const config: dotenv.DotenvConfigOutput = dotenv.config();
    if (config.error)
      throw new Error('Couldn\'t load enviroment');
    console.log(`${chalk.cyan('Enviroment')} loaded!`);
  }

}

export default EnviromentLoader;
