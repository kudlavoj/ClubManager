import { Connection } from 'mongoose';
import * as MongooseClient from 'mongoose';
import chalk from 'chalk';

class MongoConnector {

  static async connectDB(host: string): Promise<void>  {
    return new Promise((resolve, reject) => {
			try {
				MongooseClient.connect(host, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false });
				const dbConnection: Connection = MongooseClient.connection;
				dbConnection.on('error', (err) => {
					console.log(`MongoDB connection ${chalk.red('failed')}!`);
					throw err;
				});
				dbConnection.once('open', () => {
					console.log(`MongoDB ${chalk.green('successfuly')} connected!`);
					resolve();
				});
			} catch (e) {
				reject(e);
			}
    });
  }

	static async disconnectDB(): Promise<void> {
		return MongooseClient.disconnect();
	}

	static isConnected(): boolean {
		return MongooseClient.connection.readyState === 1;
	}

}

export default MongoConnector;
