import { EnviromentLoader, MongoConnector } from './connectors';
import AppServer from './AppServer';

(async () => {
	try {
		console.clear();
		await EnviromentLoader.loadEnviroment();
		await MongoConnector.connectDB(process.env.MONGO_DB_HOST);
		const appServer: AppServer = new AppServer(Number(process.env.SERVER_PORT));
		await appServer.start();
	} catch (e) {
		if (MongoConnector.isConnected())
			await MongoConnector.disconnectDB();
		console.error(e);
		process.exit(1);
	}
})();
